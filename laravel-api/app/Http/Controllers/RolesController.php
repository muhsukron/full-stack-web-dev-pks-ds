<?php

namespace App\Http\Controllers;

use App\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Roles::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data roles',
            'data'    => $roles  
        ], 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //set validation
         $validator = Validator::make($request->all(), [
            'name'   => 'required',
           
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $roles = Roles::create([
            'name'     => $request->name,
            
        ]);

        //success save to database
        if($roles) {

            return response()->json([
                'success' => true,
                'message' => 'roles Created',
                'data'    => $roles  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'roles Failed to Save',
        ], 409);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
       
       
        if ($roles) {
            return response()->json([
                'success' => true,
                'message' => 'Data role berhasil ditampilkan',
                'data'    => $roles
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();


        $validator = Validator::make($allRequest, [
            'name'   => 'required',
            
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find roles by ID
        $roles = Roles::find($id);

        if($roles) {

            //update roles
            $roles->update([
                'name'     => $request->name,
                
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data dengan id : ' . $id . ' berhasil di update',
                'data'    => $roles  
            ], 200);

        }

        //data roles not found
        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $roles = Roles::find($id);

        if($roles) {

            //delete roles
            $roles->delete();

            return response()->json([
                'success' => true,
                'message' => 'roles Deleted',
            ], 200);

        }

        //data roles not found
        return response()->json([
            'success' => false,
            'message' => 'roles Not Found',
        ], 404);
    }
    
}
