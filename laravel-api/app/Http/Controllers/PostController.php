<?php

namespace App\Http\Controllers;


use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{  
    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store' , 'update' , 'delete']);
    }
   /**
     * index
     *
     * 
     */
    public function index()
    {
        //get data from table posts
        $posts = Post::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Data daftar post berhasil ditampilkan',
            'data'    => $posts  
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        $post = Post::find($id);

        if($post)
        {
            return response()->json([
                'success' => true,
                'message' => 'Data post berhasil ditampilkan',
                'data'    => $post
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);

    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {

        $allRequest = $request->all();
        //set validation
        $validator = Validator::make($allRequest , [
            'title'   => 'required',
            'description' => 'required'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = auth()->user();

        //save to database
        $post = Post::create([
            'title'     => $request->title,
            'description'   => $request->description,
            // 'user_id' => $user->id
        ]);

        //success save to database
        if($post) {

            return response()->json([
                'success' => true,
                'message' => 'Data Post berhasil dibuat',
                'data'    => $post  
            ], 200);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Data Post gagal dibuat'
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, $id)
    {
        //set validation

        $allRequest = $request->all();


        $validator = Validator::make($allRequest, [
            'title'   => 'required',
            'description' => 'required'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }



        //find post by ID
        $post = Post::find($id);

        if($post) {

            $user = auth()->user();

            if($post->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',
                ] , 403);

            }
            //update post
            $post->update([
                'title'     => $request->title,
                'description'   => $request->description,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data dengan judul : ' . $post->title . '  berhasil diupdate',
                'data' =>    $post
            ]);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $post = Post::find($id);

        if($post) {
            $user = auth()->user();

            if ($post->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',
                ], 403);
            }

            //delete post
            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data post berhasil dihapus',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }
}
