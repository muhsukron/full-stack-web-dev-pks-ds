<?php

namespace App\Http\Controllers;


use App\Comments;
use App\Mail\PostAuthorMail;
// use App\Events\CommentsStoredEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommentsController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth:api')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $posts_id = $request->posts_id;

        $comments = Comments::where('posts_id' , $posts_id)->latest()->get();
        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Data daftar comments berhasil ditampilkan',
            'data'    => $comments  
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'content' => 'required',
            'posts_id' => 'required'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $comments = Comments::create([
            'content'     => $request->content,
            'posts_id'   => $request->posts_id,
        ]);

        // event(new CommentsStoredEvent($comments));
        Mail::to($comments->post->users->email)->send(new PostAuthorMail($comments));

        //success save to database
        if($comments) {

            return response()->json([
                'success' => true,
                'message' => 'Data Comment berhasil dibuat',
                'data'    => $comments  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Data Comment gagal dibuat',
        ], 409);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comments = Comments::find($id);

        //make response JSON
        
        if ($comments) {
            return response()->json([
                'success' => true,
                'message' => 'Data comment berhasil ditampilkan',
                'data'    => $comments
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();


        $validator = Validator::make($allRequest, [
            'content'   => 'required',
           
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find comments by ID
        $comments = Comments::find($id);

        if($comments) {

            $user = auth()->user();

            if ($comments->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',
                ], 403);
            }

            //update comments
            $comments->update([
                'content'    => $request->content,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data comment berhasil di update',
                'data'    => $comments  
            ]);

        }

        //data comments not found
        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comments = Comments::find($id);

        if($comments) {

            $user = auth()->user();

            if ($comments->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',
                ], 403);
            }

            //delete comments
            $comments->delete();

            return response()->json([
                'success' => true,
                'message' => 'comments Deleted',
            ], 200);

        }

        //data comments not found
        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }
}
