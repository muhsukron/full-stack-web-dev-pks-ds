// Let + Const
    let x = 1;
    
    if (x === 1) {
    let x = 2;
    
    console.log(x);
    // expected output: 2
    }
    
    console.log(x); // 1 

    const number = 42;
    // number = 100; // Uncaught TypeError: Assignment to constant variable.
    console.log(x);





// Arrow Functions untuk VARIABEL
    const myFunction = (nama, tinggal) => {
    console.log("Hello, " + nama + ", Domisili : " + tinggal);
    }
    // panggil Function
    myFunction("sukron", "pangkalpinang")



// Arrow Functions untuk return Value
    const myFunction1 = (name, address) => {
        return "Hello, " + name + ", Domisili : " + address;
    };
    
    // panggil Function
    console.log(myFunction1("sukron", "pangkalpinang"));




// Default Parameters
    function multiply(a, b = 1) {
        console.log(b);
        return a * b;
    }
    
    console.log(multiply(5, 2)); // expected output: 10    
    
    console.log(multiply(5));  // expected output: 5  
   

// Template Literals
    const myFunction2 = (nama, tinggal = "pangkalpinang") => { // = "pangkalpinang" adalah default value
    console.log(`Hello, ${nama}, Domisili : ${tinggal}`);
    };
    // panggil Function
    myFunction2("sukron");


// Enhanced object literals

    const fullName = 'John Doe';
    const domisili = 'pangkalpinang';
    
    const john = {fullName, domisili};
    console.log(john);

// Destructuring
        // array
        let numbers = [1,2,3]

        const [numberOne, numberTwo, numberThree] = numbers

        console.log(numberOne, numberThree)

        // object
        var studentName = {
            firstName: 'Peter',
            lastName: 'Parker'
        };
        
        const {firstName, lastName} = studentName

        console.log(firstName, lastName)

// Rest Parameters + Spread Operator

        //Rest Parameters
        
        //first example
        let scores = ['98', '95', '93', '90', '87', '85']
        let [first, second, third, ...restOfScores] = scores;
        
        console.log(first) // 98
        console.log(second) // 95
        console.log(third) // 93
        console.log(restOfScores) // [90, 87, 85] 

        //second example 
        const filter = (...rest) =>{
            return rest.filter(el => el.text !== undefined)//el=elemen(syntaxnya boleh diganti)
        }

        console.log(filter(1, {text: "wonderful"}, "next"))

        // spread operator
        let array1 = ['one', 'two']
        let array2 = ['three', 'four']
        let array3 = ['five', 'six']
        
        // ES5 Way / Normal Javascript
        
        // var combinedArray = array1.concat(array2).concat(array3)
        // console.log(combinedArray) // ['one', 'two', 'three', 'four', 'five', 'six']
        
        // ES6 Way 
        
        let combinedArray = [...array1, ...array2, ...array3]
        console.log(combinedArray) // ['one', 'two', 'three', 'four', 'five', 'six']


