// Soal 1
    // buatlah fungsi menggunakan arrow function luas dan keliling persegi panjang 
    // dengan arrow function lalu gunakan let atau const di dalam soal ini

    const keliling = (panjang, lebar) => {
        console.log();
        return (panjang + lebar) * 2;
    };    
   
    console.log(keliling(10, 8));



// Soal 2
    const newFunction = (firstName, lastName) => {
            console.log(firstName + " " + lastName);
        }
    
    newFunction("William", "Imoh")


// Soal 3
    // Diberikan sebuah objek sebagai berikut:
    const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
    }

    // Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)
    const {firstName, lastName, address, hobby} =  newObject

    // Driver code
    console.log(firstName, lastName, address, hobby)



// Soal 4
    // Kombinasikan dua array berikut menggunakan array spreading ES6
    const west = ["Will", "Chris", "Sam", "Holly"]
    const east = ["Gill", "Brian", "Noel", "Maggie"]
    const combined = [...west, ...east]
    //Driver Code
    console.log(combined)


// Soal 5

    const planet = "earth" 
    const view = "glass" 
    const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}` 

    console.log(before);