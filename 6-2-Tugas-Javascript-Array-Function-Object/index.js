
//soal 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]
var hewan = daftarHewan.sort();
for(var i = 0; i < hewan.length; i++){
    console.log(hewan[i]);
}




//soal 2

function introduce(param){
    return "Nama saya " + param.name + ", umur saya " + param.age + ", alamat saya di " + param.address + ", dan saya punya hobi yaitu " + param.hobby
       
}
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 





//soal 3

function hitung_huruf_vokal(param){
    var text = param.toLowerCase();
    var jumlah = 0;
    for(var i = 0; i < text.length; i ++){
        switch(text[i]){
            case 'a':
                jumlah++
                break;
            case 'i':
                jumlah++
                break;    
            case 'u':
                jumlah++
                break;
            case 'e':
                jumlah++
                break; 
            case 'o':  
                jumlah++
                break;  
                
            default:
                break;                
        }
    }
    return jumlah
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2




//soal 4
function hitung(param){
    param = -2 + (param * 2)    
    return param;
}


console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8