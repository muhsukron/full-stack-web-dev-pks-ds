var sayHello = "Hello World!" 
console.log(sayHello)


var hobbies = ["coding", "cycling", "climbing", "skateboarding"] 

hobbies.push("sleeping" , "swimming") //menambah data
hobbies.pop() //menghilangkan data terakhir "swimming"

console.log(hobbies)


var numbers = [0, 1, 2, 3]
numbers.unshift(-1) 
numbers.shift()
console.log(numbers) // [-1, 0, 1, 2, 3] ->unshift
                     // [ 0, 1, 2, 3 ]  ->shift


var animals = ["kera", "gajah", "musang"] 
animals.sort()
console.log(animals) // ["gajah", "kera", "musang"]    


var angka = [0, 1, 2, 3]
var irisan1 = angka.slice(1,3) 
console.log(irisan1) //[1, 2]
var irisan2 = angka.slice(0,2)
console.log(irisan2) //[0, 1] 


var fruits = [ "banana", "orange", "grape"] 
fruits.splice(1, 1, "watermelon") //menghapus 1 (orange) diganti watermelon
console.log(fruits) // [ "banana", "watermelon", "grape"]


var biodata = "name:john,doe" 
var name = biodata.split(":")
console.log(name) // [ "name", "john,doe"] 

var biodata = " Muhammad Sukron " 
var nama = biodata.trim().split(" ") //parameter spasi|| trim->agar length spasi tidak di hitung
console.log(nama) //[ 'Muhammad', 'Sukron' ]

var biodata = " Muhammad Sukron " 
var names = biodata.split("a") 
console.log(names.length) //[ ' Muh', 'mm', 'd Sukron ' ] | length 3


var title = ["my", "first", "experience", "as", "programmer"] 
var slug = title.join("-")
console.log(slug) // "my-first-experience-as-programmer"
var sluger = title.join(" ")
console.log(sluger)// my first experience as programmer

//function tanpa return
  function tampilkan() {
    console.log("halo!");
  }
   
  tampilkan(); 

//function dengan return
  function tampilkan1() {
    var a = 2
    var b = 3

    return a + b
  }   
 console.log(tampilkan1()); 
//function dengan parameter
 function kali(a){
    return a * 3
}
var tampung = kali(9);
console.log(tampung) 


//function dengan parameter lebih dari satu
 function jumlah(a , b){
     return a + b
 }

 console.log(jumlah(3 , 5));


//inisiasi parameter dengan nilai default

function operasi (a , b , jenis = "penjumlahan"){

    var c

   if(jenis == "penjumlahan"){
       c = a + b
   }else if( jenis == "pengurangan"){
       c = a - b
   }

   return c
}

console.log(operasi(3 , 5 , "pengurangan"));//-2
console.log(operasi(3 , 5));//8

//object

var biodata = {
    nama : "Muhammad Sukron",
    alamat : "Bangka"
}

console.log(biodata)

//deklarasi object
var car2 = {}
// meng-assign key:value dari object car2
car2.brand = "Lamborghini"
car2.type = "Sports Car"
car2.price = 100000000 
car2["horse power"] = 730 
 
console.log(car2)


//array of object
//forEach
var mobil = [
    {merk: "BMW", warna: "merah", tipe: "sedan"}, 
    {merk: "toyota", warna: "hitam", tipe: "box"}, 
    {merk: "audi", warna: "biru", tipe: "sedan"}
]

mobil.forEach(function(item){
    item.tipe = "sedan"// merubah tipe jadi sedan semua
    // console.log(item)
    console.log(item.tipe)// merubah tipe jadi sedan semua

})

console.log(mobil)

//filter
var arrayMobilFilter = mobil.filter(function(itemz){
    return itemz.warna != "hitam";
 })
 
 console.log(arrayMobilFilter)

//map
var cars = [
    {merk: "BMW", warna: "merah", tipe: "sedan"}, 
    {merk: "toyota", warna: "hitam", tipe: "box"}, 
    {merk: "audi", warna: "biru", tipe: "sedan"}
]

var colors = cars.map(function(car) {
  return car.warna  
})

var newCars = cars.map(function(items){
    return {merk: items.merk, warna: items.warna, tipe:"sedan"}
})

console.log(newCars)

console.log(colors)






//log
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]

console.log(daftarHewan)
console.log(daftarHewan[1])
console.log(daftarHewan[daftarHewan.length -1])


//soal 4
function hitung($angka){

    var a = $angka - 2

    return a
}

console.log(hitung(0))
console.log(hitung(1))