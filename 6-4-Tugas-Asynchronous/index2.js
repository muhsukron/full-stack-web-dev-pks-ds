const readBooks = require('./callback.js')
var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

 
// Lanjutkan code untuk menjalankan function readBooksPromise 
readBooksPromise(10000,books[0],function(sisa1){
    readBooksPromise(sisa1,books[1],function(sisa2){
        readBooksPromise(sisa2,books[2],function(sisa3){
            readBooksPromise(sisa3,books[3],function(){

            })

        })     

    })
    
})

readBooks()
     .then(books =>{
    console.log(books)
})
     .catch(error => console.log(error))
